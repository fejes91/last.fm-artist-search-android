package com.adamfejes.ui.main

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.*
import com.adamfejes.entity.Artist
import com.adamfejes.repo.ArtistRepo
import com.adamfejes.ui.artistDetails.ArtistDetailsViewModel
import com.adamfejes.ui.artistDetails.ArtistDetailsViewModelImpl
import junit.framework.Assert
import org.junit.Before

import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import java.lang.Exception

class MainViewModelImplTest {
    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    companion object {
        private const val MBID = "mbid"
        private const val NAME = "artistName"
        private const val BIO = "bio bio bio bio bio "
        private const val LISTENERS = 5
        private const val IMAGEURL = "imageurl"

        private val ARTIST_DETAILS = Artist(MBID, IMAGEURL, LISTENERS, NAME, BIO)
        private val IS_LOADING = true
        private val EXCEPTION = Exception("TEST")

        private val artistDetailsLiveData: MutableLiveData<Artist> = MutableLiveData()
        private val isLoadingLiveData: MutableLiveData<Boolean> = MutableLiveData()
        private val hasErrorLiveData: MutableLiveData<Throwable> = MutableLiveData()
    }

    private lateinit var underTest : ArtistDetailsViewModel

    @Mock
    private lateinit var lifecycleOwner: LifecycleOwner
    @Mock
    private lateinit var artistRepo: ArtistRepo

    private fun setupLifecycleMocking() {
        val lifecycle = LifecycleRegistry(lifecycleOwner)
        lifecycle.markState(Lifecycle.State.RESUMED)
        Mockito.`when`(lifecycleOwner.lifecycle).thenReturn(lifecycle)
    }

    private fun setupRepoMocking(){
        artistDetailsLiveData.value = ARTIST_DETAILS
        isLoadingLiveData.value = IS_LOADING
        hasErrorLiveData.value = EXCEPTION
        Mockito.`when`<LiveData<Artist>>(artistRepo.getArtistDetails()).thenReturn(artistDetailsLiveData)
        Mockito.`when`<LiveData<Boolean>>(artistRepo.getLoading()).thenReturn(isLoadingLiveData)
        Mockito.`when`<LiveData<Throwable>>(artistRepo.getError()).thenReturn(hasErrorLiveData)
    }

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        setupLifecycleMocking()
        setupRepoMocking()
        underTest = ArtistDetailsViewModelImpl(ARTIST_DETAILS, artistRepo)
    }

    @Test
    fun testArtistName() {
        Assert.assertEquals(underTest.getName(), NAME)
    }

    @Test
    fun testArtistArtwork() {
        Assert.assertEquals(underTest.getImageUrl(), IMAGEURL)
    }

    @Test
    fun testArtistBio() {
        Assert.assertEquals(underTest.getArtistDetails().value, ARTIST_DETAILS)
    }
}