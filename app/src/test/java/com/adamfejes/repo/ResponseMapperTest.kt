package com.adamfejes.repo

import com.adamfejes.network.*
import junit.framework.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ResponseMapperTest {
    companion object {
        private val IMAGE1 = Image("imageurl1")
        private val IMAGE2 = Image("imageurl2")
        private val IMAGE3 = Image("imageurl3")
        private val NAME = "ARTIST1"

        private val artistResponseList = ArrayList<ArtistResponse>()
        private val artists = ArtistMatches(artistResponseList)
        private val result = Results(artists = artists)
        private val MOCK_SEARCH_RESPONSE = ArtistSearchResponse(result)

        private lateinit var artist1 : ArtistResponse
        private lateinit var artist2  : ArtistResponse
        private lateinit var artist3  : ArtistResponse
    }

    private lateinit var underTest: ResponseMapper

    @Before
    fun setUp() {
        artist1 = ArtistResponse()
        artist2 = ArtistResponse()
        artist3 = ArtistResponse()

        underTest = ResponseMapper()
        artistResponseList.clear()
    }

    @Test
    fun testOutputCountIsEqualWithInputCount() {
        artistResponseList.addAll(listOf(artist1, artist2, artist3))
        val result = underTest.mapArtistSearchResponseToArtists(MOCK_SEARCH_RESPONSE)
        Assert.assertEquals(result.size, artistResponseList.size)
    }

    @Test
    fun testOutputCountIsZeroOnEmptyInput() {
        val result = underTest.mapArtistSearchResponseToArtists(MOCK_SEARCH_RESPONSE)
        Assert.assertEquals(result.size, 0)
    }

    @Test
    fun outputArtistNameTest() {
        artist1 = ArtistResponse(name = NAME)
        artistResponseList.add(artist1)
        val result = underTest.mapArtistSearchResponseToArtists(MOCK_SEARCH_RESPONSE)
        Assert.assertEquals(result[0].name, NAME)
    }

    @Test
    fun outputArtistArtworkIsTheLastOfInputTest() {
        artist1 = ArtistResponse(image = listOf(IMAGE1, IMAGE2, IMAGE3))
        artistResponseList.add(artist1)
        val result = underTest.mapArtistSearchResponseToArtists(MOCK_SEARCH_RESPONSE)
        Assert.assertEquals(result[0].imageUrl, IMAGE3.text)
    }

    //TODO test more attributes

    /**TODO test [ArtistDetailsResponse] mapping **/

}