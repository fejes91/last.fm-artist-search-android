package com.adamfejes.network

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Retrofit API interface
 * Created by Adam Fejes on 2018. 03. 26..
 */

interface ApiInterface {

    /**
     * Network call for search artist endpoint
     * @param query Search query string
     * @return Returns an [ArtistSearchResponse] object wrapped to [Observable]
     */
    @GET(PATH)
    fun searchForArtist(@Query(PARAM_METHOD) method : String = VALUE_ARTIST_SEARCH,
                        @Query("artist") query: String): Observable<ArtistSearchResponse>

    /**
     * Network call for artist details enpoint
     * @param mbid MusicBrainz id of the artist
     * @param artistName Artist name
     * @return Returns an [ArtistDetailsResponse] object wrapped to [Observable]
     */
    @GET(PATH)
    fun getArtistDetails(@Query(PARAM_METHOD) method: String = VALUE_ARTIST_DETAILS,
                         @Query("mbid") mbid: String, @Query("artist") artistName: String): Observable<ArtistDetailsResponse>

    companion object {
        private const val PATH = "2.0"
        private const val PARAM_METHOD = "method"
        private const val VALUE_ARTIST_SEARCH = "artist.search"
        private const val VALUE_ARTIST_DETAILS = "artist.getinfo"
    }
}
