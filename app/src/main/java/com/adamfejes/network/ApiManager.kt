package com.adamfejes.network

import io.reactivex.Observable


/**
 * ApiManager [ApiInterface]
 * Created by Adam Fejes on 2018. 03. 26..
 */

interface ApiManager {
    fun getArtistDetails(mbid: String, name: String): Observable<ArtistDetailsResponse>
    fun searchArtist(queryArtist: String): Observable<ArtistSearchResponse>
}
