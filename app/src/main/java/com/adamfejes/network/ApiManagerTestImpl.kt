package com.adamfejes.network

import android.app.Application
import com.adamfejes.lastfmArtistSearch.R
import com.google.gson.Gson
import io.reactivex.Observable

class ApiManagerTestImpl(private val application: Application) : ApiManager {
    override fun getArtistDetails(mbid: String, name: String): Observable<ArtistDetailsResponse> {
        return Observable.just(Gson().fromJson(
                application.resources.openRawResource(R.raw.mock_artist1_details_response).bufferedReader(),
                ArtistDetailsResponse::class.java))
    }

    override fun searchArtist(queryArtist: String): Observable<ArtistSearchResponse> {
        return Observable.just(Gson().fromJson(
                application.resources.openRawResource(R.raw.mock_search_result_response).bufferedReader(),
                ArtistSearchResponse::class.java))
    }
}