package com.adamfejes.network

import android.content.res.Resources
import com.adamfejes.lastfmArtistSearch.R
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Builds a Retrofit instance
 * @param resources
 */
class ApiManagerImpl (resources: Resources, okHttpClient: OkHttpClient)  : ApiManager {
        private val BASE_URL: String = resources.getString(R.string.base_url)
        private val apiInterface: ApiInterface

        init {
            apiInterface = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build()
                    .create(ApiInterface::class.java)
        }

        /**
         * Call network for search artist
         * @return Observable of [Response]
         */
        override fun searchArtist(queryArtist: String): Observable<ArtistSearchResponse> {
            return apiInterface.searchForArtist(query = queryArtist)
                    .subscribeOn(Schedulers.io())
        }

        /**
         * Call network for artist details
         */
        override fun getArtistDetails(mbid: String, name: String): Observable<ArtistDetailsResponse> {
            return apiInterface.getArtistDetails(mbid = mbid, artistName = name)
                    .subscribeOn(Schedulers.io())
        }
}