package com.adamfejes.network

import com.google.gson.annotations.SerializedName

/**
 * Data class for holding artist search result network response
 * Created by Adam Fejes on 2018. 03. 26..
 */

data class ArtistSearchResponse(
    val results: Results
)

data class Results(
        @SerializedName("@attr")
    val attr: Attr? = null,
        @SerializedName("artistmatches")
    val artists: ArtistMatches,
        @SerializedName("opensearch:Query")
    val query: OpenSearchQuery? = null,
        @SerializedName("opensearch:totalResults")
    val totalResults: Int = -1,
        @SerializedName("opensearch:startIndex")
    val startIndex: Int = -1,
        @SerializedName("opensearch:itemsPerPage")
    val itemsPerPage: Int = -1
)

data class Attr(
    val `for`: String = ""
)

data class ArtistMatches(
    val artist: List<ArtistResponse> = ArrayList()
)

data class ArtistResponse(
        val image: List<Image> = ArrayList(),
        val listeners: Int = -1,
        val mbid: String = "",
        val name: String = "",
        val streamable: Boolean = false,
        val url: String = ""
)

data class Image(
    @SerializedName("#text")
    val text: String = "",
    val size: String = ""
)

data class OpenSearchQuery(
    @SerializedName("#text")
    val text: String = "",
    val role: String = "",
    val searchTerms: String = "",
    val startPage: Int = -1
)