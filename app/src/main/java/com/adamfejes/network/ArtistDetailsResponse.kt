package com.adamfejes.network
import com.google.gson.annotations.SerializedName

/**
 * Network response data class for artist details
 */
data class ArtistDetailsResponse(
    val artist: Artist
) {
    data class Artist(
        val bio: Bio,
        val image: List<Image>,
        val mbid: String,
        val name: String,
        val ontour: String,
        val stats: Stats,
        val streamable: String,
        val tags: Tags,
        val url: String
    ) {
        data class Tags(
            val tag: List<Tag>
        ) {
            data class Tag(
                val name: String,
                val url: String
            )
        }

        data class Image(
            @SerializedName("#text")
            val text: String,
            val size: String
        )

        data class Stats(
            val listeners: Int = -1,
            val playcount: String
        )

        data class Bio(
            val content: String,
            val links: Links,
            val published: String,
            val summary: String
        ) {
            data class Links(
                val link: Link
            ) {
                data class Link(
                    @SerializedName("#text")
                    val text: String,
                    val href: String,
                    val rel: String
                )
            }
        }
    }
}