package com.adamfejes.entity

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Entity class for storing artist info
 * Created by Adam_Fejes on 2018-03-26.
 */

@Parcelize
data class Artist(
        val mbid: String,
        val imageUrl: String? = null,
        val listeners: Int,
        val name: String,
        val bio: String? = null
) : Parcelable
