package com.adamfejes.repo

import com.adamfejes.entity.Artist
import com.adamfejes.network.ArtistDetailsResponse
import com.adamfejes.network.ArtistSearchResponse

/**
 * Mapping network call responses to Entity classes
 * Created by Adam Fejes on 2018. 10. 28..
 */
class ResponseMapper {

    /**
     * Maps artist search result to a list of [Artist] objects
     */
    fun mapArtistSearchResponseToArtists(response: ArtistSearchResponse) : List<Artist> {
        val artists = ArrayList<Artist>()
        for(artist in response.results.artists.artist) {
            artists.add(Artist(
                    artist.mbid,
                    if(artist.image.isNotEmpty()) artist.image[artist.image.size -1].text else "",
                    artist.listeners,
                    artist.name))
        }
        return artists
    }

    /**
     * Maps artist details response to an [Artist] object
     */
    fun mapArtistDetailsResponseToArtist(response: ArtistDetailsResponse): Artist {
        return Artist(response.artist.mbid,
                if(response.artist.image.isNotEmpty()) response.artist.image[response.artist.image.size -1].text else "",
                response.artist.stats.listeners,
                response.artist.name,
                response.artist.bio.summary)
    }
}