package com.adamfejes.repo

import android.annotation.SuppressLint
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.adamfejes.entity.Artist
import com.adamfejes.network.ApiManager
import timber.log.Timber

/**
 * Implementation class of [ArtistRepo]
 * Created by Adam Fejes on 2018. 10. 28..
 */
class ArtistRepoImpl(private val apiManager: ApiManager, private val responseMapper: ResponseMapper) : ArtistRepo {
    private val errorState = MutableLiveData<Throwable>()
    private val loadingState = MutableLiveData<Boolean>()
    private val artistList = MutableLiveData<List<Artist>>()
    private val artistDetails = MutableLiveData<Artist>()

    /**
     * Request network search. Result can be observed through [getSearchResult]
     */
    @SuppressLint("CheckResult")
    override fun requestArtistSearch(queryArtist: String) {
        loadingState.value = true
        apiManager.searchArtist(queryArtist)
                .map { responseMapper.mapArtistSearchResponseToArtists(it) }
                .subscribe (
                        {
                            artistList.postValue(it)
                        },
                        { Timber.e(it); errorState.postValue(it)}, {loadingState.postValue(false) })
    }

    /**
     * Request network artist data. Result can be observed through [getArtistDetails]
     */
    @SuppressLint("CheckResult")
    override fun requestArtistDetails(mbid: String, name: String) {
        loadingState.value = true
        apiManager.getArtistDetails(mbid, name)
                .map{ responseMapper.mapArtistDetailsResponseToArtist(it) }
                .subscribe({
                    artistDetails.postValue(it)
                }, {Timber.e(it); errorState.postValue(it)}, {loadingState.postValue(false)})
    }

    /**
     * Observable [LiveData] contains network call results from [requestArtistSearch]
     */
    override fun getSearchResult(): LiveData<List<Artist>> {
        return artistList
    }

    /**
     * Observable [LiveData] contains network call results from [requestArtistDetails]
     */
    override fun getArtistDetails(): LiveData<Artist> {
        return artistDetails
    }

    /**
     * Observable [LiveData] contains network call errors
     */
    override fun getError(): LiveData<Throwable> {
        return errorState
    }

    /**
    * Observable [LiveData] contains network call loading state
    */
    override fun getLoading(): LiveData<Boolean> {
        return loadingState
    }
}