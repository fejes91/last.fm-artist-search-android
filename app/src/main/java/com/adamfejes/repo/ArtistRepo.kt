package com.adamfejes.repo

import android.arch.lifecycle.LiveData
import com.adamfejes.entity.Artist
/**
 * Data layer of artist search use case
 * Created by Adam Fejes on 2018. 10. 28..
 */
interface ArtistRepo {
    fun requestArtistSearch(queryArtist: String)
    fun requestArtistDetails(mbid : String = "", name : String)

    fun getSearchResult() : LiveData<List<Artist>>
    fun getArtistDetails() : LiveData<Artist>
    fun getError() : LiveData<Throwable>
    fun getLoading(): LiveData<Boolean>
}