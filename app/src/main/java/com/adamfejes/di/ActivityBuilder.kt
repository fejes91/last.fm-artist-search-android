package com.adamfejes.di

import com.adamfejes.ui.artistDetails.ArtistDetailsActivity
import com.adamfejes.ui.main.MainActivity

import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Activity builder Dagger module
 * Created by Adam Fejes on 2018. 02. 06..
 */

@Module
abstract class ActivityBuilder {
    @ContributesAndroidInjector(modules = arrayOf(MainModule::class))
    internal abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector(modules = arrayOf(DetailsModule::class))
    internal abstract fun bindDetailsActivity(): ArtistDetailsActivity
}
