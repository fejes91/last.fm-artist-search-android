package com.adamfejes.di

import android.app.Application
import com.adamfejes.Utils
import com.adamfejes.lastfmArtistSearch.R
import com.adamfejes.network.ApiManager
import com.adamfejes.network.ApiManagerImpl
import com.facebook.stetho.okhttp3.StethoInterceptor
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import javax.inject.Named
import javax.inject.Singleton

@Module
class RemoteModule {

    private val PARAM_API_KEY = "api_key"
    private val PARAM_FORMAT = "format"
    private val VALUE_JSON = "json"

    companion object {
        private const val INTERCEPTOR_PARAM = "paramInterceptor"
        private const val INTERCEPTOR_CACHE = "cahceInterceptor"
        private const val cacheSize = 10 * 1024 * 1024.toLong() // 10MB
    }

    @Singleton
    @Provides
    open fun provideApiManager(application: Application, okHttpClient: OkHttpClient): ApiManager {
        return ApiManagerImpl(application.resources, okHttpClient)
    }

    @Singleton
    @Provides
    internal fun provideHttpClient(@Named(INTERCEPTOR_PARAM) paramInterceptor: Interceptor,
                                   @Named(INTERCEPTOR_CACHE) cacheInterceptor: Interceptor,
                                   cache: Cache) : OkHttpClient {
        return OkHttpClient.Builder()
                .addInterceptor(paramInterceptor)
                .addInterceptor(cacheInterceptor)
                .addNetworkInterceptor(StethoInterceptor())
                .cache(cache)
                .build()
    }

    @Singleton
    @Provides
    @Named(INTERCEPTOR_PARAM)
    internal fun provideParameterInterceptor(application: Application) : Interceptor {
        return Interceptor {
            chain ->
            val original = chain.request()
            val originalHttpUrl = original.url()
            val url = originalHttpUrl.newBuilder()
                    .addQueryParameter(PARAM_API_KEY, application.getString(R.string.api_key))
                    .addQueryParameter(PARAM_FORMAT, VALUE_JSON)
                    .build()
            chain.proceed(original.newBuilder().url(url).build())
        }
    }

    @Singleton
    @Provides
    internal fun provideCache(application: Application) : Cache {
        return Cache(application.cacheDir, cacheSize)
    }

    @Singleton
    @Provides
    @Named(INTERCEPTOR_CACHE)
    internal fun provideCacheInterceptor(application: Application, utils: Utils) : Interceptor {
        return Interceptor {
            chain ->
            var request = chain.request()
            request = if(utils.isOnline(application))
                request.newBuilder().header("Cache-Control", "public, max-age=60").build()
            else
                request.newBuilder().header("Cache-Control", "public, only-if-cached, max-stale=${60 * 60 * 24 * 7}").build()
            chain.proceed(request)
        }
    }
}