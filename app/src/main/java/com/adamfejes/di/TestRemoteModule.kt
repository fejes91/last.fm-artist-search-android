package com.adamfejes.di

import android.app.Application
import com.adamfejes.network.ApiManager
import com.adamfejes.network.ApiManagerTestImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class TestRemoteModule {

    @Singleton
    @Provides
    open fun provideApiManager(application: Application): ApiManager {
        return ApiManagerTestImpl(application)
    }
}