package com.adamfejes.di

import android.app.Application
import com.adamfejes.Utils
import com.adamfejes.lastfmArtistSearch.R
import com.adamfejes.network.ApiManager
import com.adamfejes.network.ApiManagerImpl
import com.adamfejes.repo.ArtistRepo
import com.adamfejes.repo.ArtistRepoImpl
import com.adamfejes.repo.ResponseMapper
import com.facebook.stetho.okhttp3.StethoInterceptor
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.OkHttpClient.Builder
import javax.inject.Named
import javax.inject.Singleton

/**
 * Application level Dagger module
 * Created by Adam Fejes on 2018. 02. 06..
 */

@Module(subcomponents = arrayOf(MainComponent::class))
open class AppModule {

    @Singleton
    @Provides
    internal fun provideUtils(): Utils {
        return Utils()
    }

    @Singleton
    @Provides
    internal fun provideArtistSearchRepo(apiManager: ApiManager, responseMapper: ResponseMapper) : ArtistRepo {
        return ArtistRepoImpl(apiManager, responseMapper)
    }

    @Singleton
    @Provides
    internal fun provideResponseMapper() : ResponseMapper {
        return ResponseMapper()
    }
}
