package com.adamfejes.di

import android.app.Application
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.adamfejes.entity.Artist
import com.adamfejes.repo.ArtistRepo
import com.adamfejes.ui.artistDetails.ArtistDetailsActivity
import com.adamfejes.ui.artistDetails.ArtistDetailsViewModelImpl
import dagger.Module
import dagger.Provides

/**
 * Dagger module for main scree
 */
@Module
class DetailsModule {
    @Provides
    internal fun provideDetailsViewModelFactory(activity: ArtistDetailsActivity, artistRepo: ArtistRepo) : ViewModelProvider.Factory {
        return object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                val artist = activity.intent.extras.get(ArtistDetailsActivity.ARG_ARTIST) as Artist
                return ArtistDetailsViewModelImpl(artist, artistRepo) as T
            }
        }
    }
}