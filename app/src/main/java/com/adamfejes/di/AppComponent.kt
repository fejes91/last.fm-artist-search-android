package com.adamfejes.di

import android.app.Application

import com.adamfejes.LastFMSearchApp

import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import dagger.android.support.DaggerApplication
import javax.inject.Singleton

/**
 * Application level Dagger component
 * Created by Adam Fejes on 2018. 02. 06..
 */

@Singleton
@Component(modules = arrayOf(
        AndroidSupportInjectionModule::class,
        AppModule::class,
        RemoteModule::class,
        ActivityBuilder::class))
interface AppComponent : AndroidInjector<DaggerApplication> {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(app: LastFMSearchApp)
}
