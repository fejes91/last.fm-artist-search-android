package com.adamfejes.di

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.adamfejes.repo.ArtistRepo
import com.adamfejes.ui.main.MainViewModelImpl
import dagger.Module
import dagger.Provides

/**
 * Dagger module for main screen
 * Created by Adam Fejes on 2018. 02. 06..
 */

@Module
class MainModule {
    @Provides
    internal fun provideMainViewModelFactory(artistRepo: ArtistRepo) : ViewModelProvider.Factory {
        return object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return MainViewModelImpl(artistRepo) as T
            }
        }
    }
}
