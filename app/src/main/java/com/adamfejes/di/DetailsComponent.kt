package com.adamfejes.di

import com.adamfejes.ui.artistDetails.ArtistDetailsActivity
import com.adamfejes.ui.main.MainActivity
import dagger.Subcomponent
import dagger.android.AndroidInjector

/**
 * Dagger component for details screen
 * Created by Adam Fejes on 2018. 02. 06..
 */

@Subcomponent(modules = arrayOf(DetailsModule::class))
interface DetailsComponent : AndroidInjector<ArtistDetailsActivity> {
    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<ArtistDetailsActivity>()
}
