package com.adamfejes.di

import com.adamfejes.ui.main.MainActivity
import dagger.Subcomponent
import dagger.android.AndroidInjector
import javax.inject.Singleton

/**
 * Dagger component for main screen
 * Created by Adam Fejes on 2018. 02. 06..
 */


@Subcomponent(modules = arrayOf(MainModule::class))
interface MainComponent : AndroidInjector<MainActivity> {
    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<MainActivity>()
}
