package com.adamfejes.ui.main

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.support.v7.widget.SearchView
import com.adamfejes.entity.Artist

/**
 * Viewmodel interface for the main screen
 * Created by Adam Fejes on 2018. 03. 26..
 */

abstract class MainViewModel : ViewModel() {
    abstract fun getQueryTextListener() : SearchView.OnQueryTextListener

    abstract fun getLoading() : LiveData<Boolean>

    abstract fun getSearchResult(): LiveData<List<Artist>>

    abstract fun getError(): LiveData<Throwable>
}
