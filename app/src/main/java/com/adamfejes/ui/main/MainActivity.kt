package com.adamfejes.ui.main

import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.adamfejes.entity.Artist
import com.adamfejes.lastfmArtistSearch.R
import com.adamfejes.ui.main.artistList.ArtistAdapter
import com.adamfejes.ui.main.artistList.ArtistViewModel
import com.adamfejes.ui.main.artistList.ArtistViewModelImpl
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject


/**
 * Activity for main screen
 */
class MainActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var mainViewModelFactory: ViewModelProvider.Factory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val viewModel = ViewModelProviders.of(this, mainViewModelFactory).get(MainViewModel::class.java)
        initViews(viewModel)
        initObservers(viewModel)
    }

    private fun initViews(viewModel: MainViewModel) {
        artistList.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        artistList.adapter = ArtistAdapter()
        searchView.setOnQueryTextListener(viewModel.getQueryTextListener())
        searchView.requestFocus()
    }

    private fun initObservers(viewModel: MainViewModel) {
        viewModel.getSearchResult().observe(this, Observer<List<Artist>> {
            if(it != null) {
                (artistList.adapter as ArtistAdapter).setItems(generateItems(it))
                mainContainer.requestFocus()
                hideKeyBoard()
            }
        })

        viewModel.getError().observe(this, Observer<Throwable> {
            Toast.makeText(application, "Error occured: ${it?.message ?: "Unknown"}", Toast.LENGTH_SHORT).show()
            //TODO display error state
        })

        viewModel.getLoading().observe(this, Observer<Boolean> {loading ->
            progressBar.visibility = if(loading == true) View.VISIBLE else View.GONE
            artistList.visibility = if(loading == false) View.VISIBLE else View.GONE
        })
    }

    private fun generateItems(artists : List<Artist>): ArrayList<ArtistViewModel> {
        val viewModels = ArrayList<ArtistViewModel>()
        for (artist in artists) {
            viewModels.add(ArtistViewModelImpl(application, artist))
        }
        return viewModels
    }

    private fun hideKeyBoard(){
        val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(mainContainer.windowToken, 0)
    }
}
