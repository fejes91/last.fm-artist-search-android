package com.adamfejes.ui.main.artistList

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import com.adamfejes.lastfmArtistSearch.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.artist_item.view.*


/**
 * Adapter to display artist search results on main screen
 * Created by Adam Fejes on 2018. 10. 28..
 */
class ArtistAdapter : RecyclerView.Adapter<ViewHolder>() {
    private var items = ArrayList<ArtistViewModel>()

    fun setItems(itemViewModels: ArrayList<ArtistViewModel>) {
        items = itemViewModels
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent?.context)
        return ViewHolder(layoutInflater.inflate(R.layout.artist_item, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        holder?.bind(items[position])
    }
}

class ViewHolder(view : View) : RecyclerView.ViewHolder(view){
    fun bind(artist : ArtistViewModel) {
        itemView.artistName.text = artist.getName()
        Picasso.with(itemView.context).load(artist.getImageUrl()).placeholder(R.drawable.app_icon).into(itemView.backgroundImage)
        itemView.setOnClickListener {artist.onClick()}
    }
}