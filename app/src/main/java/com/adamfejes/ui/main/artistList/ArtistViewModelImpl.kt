package com.adamfejes.ui.main.artistList

import android.app.Application
import com.adamfejes.entity.Artist
import com.adamfejes.ui.artistDetails.ArtistDetailsActivity

/**
 * Implementation of [ArtistViewModel]
 * Created by Adam Fejes on 2018. 10. 28..
 */
open class ArtistViewModelImpl(application: Application, private val artist : Artist) : ArtistViewModel(application) {

    override fun getName(): String {
        return artist.name
    }

    override fun getImageUrl(): String? {
        return if(artist.imageUrl.isNullOrEmpty()) null else artist.imageUrl
    }

    override fun onClick() {
        ArtistDetailsActivity.launch(getApplication(), artist)
    }
}