package com.adamfejes.ui.main.artistList

import android.app.Application
import android.arch.lifecycle.AndroidViewModel

/**
 * Viewmodel interface for search result list items
 * Created by Adam Fejes on 2018. 10. 28..
 */
abstract class ArtistViewModel(application: Application) : AndroidViewModel(application) {
    abstract fun getName() : String
    abstract fun getImageUrl() : String?
    abstract fun onClick()
}