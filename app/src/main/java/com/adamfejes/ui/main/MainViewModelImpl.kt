package com.adamfejes.ui.main

import android.arch.lifecycle.LiveData
import android.support.v7.widget.SearchView
import com.adamfejes.entity.Artist
import com.adamfejes.repo.ArtistRepo

/**
 * Implementation for [MainViewModel]
 * Created by Adam Fejes on 2018. 03. 26..
 */

class MainViewModelImpl(private val artistRepo: ArtistRepo) : MainViewModel() {
    private val queryListener: SearchView.OnQueryTextListener
    init {
        queryListener = object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if(query?.isNotEmpty() == true) {
                    artistRepo.requestArtistSearch(query)
                } else {
                    //TODO handle empty query
                }
                return true
            }

            override fun onQueryTextChange(p0: String?): Boolean{
                return false
            }
        }
    }

    override fun getSearchResult(): LiveData<List<Artist>> {
        return artistRepo.getSearchResult()
    }

    override fun getError(): LiveData<Throwable> {
        return artistRepo.getError()
    }

    override fun getQueryTextListener(): SearchView.OnQueryTextListener {
        return queryListener
    }

    override fun getLoading() : LiveData<Boolean> {
        return artistRepo.getLoading()
    }
}
