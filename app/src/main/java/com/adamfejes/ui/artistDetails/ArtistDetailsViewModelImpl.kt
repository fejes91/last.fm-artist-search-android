package com.adamfejes.ui.artistDetails

import android.app.Application
import android.arch.lifecycle.LiveData
import com.adamfejes.entity.Artist
import com.adamfejes.repo.ArtistRepo

/**
 * Implementation of [ArtistDetailsViewModel]
 */
class ArtistDetailsViewModelImpl(private val artist: Artist, private val artistRepo: ArtistRepo) : ArtistDetailsViewModel() {
    override fun getName(): String {
        return artist.name
    }

    override fun getImageUrl(): String? {
        return if(artist.imageUrl.isNullOrEmpty()) null else artist.imageUrl
    }

    override fun getArtistDetails(): LiveData<Artist> {
        return artistRepo.getArtistDetails()
    }

    override fun getErrorState() : LiveData<Throwable> {
        return artistRepo.getError()
    }

    override fun getLoadingState(): LiveData<Boolean> {
        return artistRepo.getLoading()
    }

    override fun requestArtistDetails() {
        artistRepo.requestArtistDetails(artist.mbid, artist.name)
    }
}