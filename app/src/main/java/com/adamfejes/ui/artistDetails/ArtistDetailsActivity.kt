package com.adamfejes.ui.artistDetails

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Html
import android.text.method.LinkMovementMethod
import android.view.View
import android.widget.Toast
import com.adamfejes.entity.Artist
import com.adamfejes.lastfmArtistSearch.R
import com.squareup.picasso.Picasso
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_artist_details.*
import javax.inject.Inject

/**
 * Artist details screen
 */
class ArtistDetailsActivity : DaggerAppCompatActivity() {
    companion object {
        const val ARG_ARTIST = "artist"
        fun launch(context: Context, artist: Artist) {
            val bundle = Bundle()
            bundle.putParcelable(ARG_ARTIST, artist)
            val intent = Intent(context, ArtistDetailsActivity::class.java)
            intent.putExtras(bundle)
            context.startActivity(intent)
        }
    }

    @Inject
    lateinit var artistViewModelFactory: ViewModelProvider.Factory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_artist_details)

        val viewModel = ViewModelProviders.of(this, artistViewModelFactory).get(ArtistDetailsViewModel::class.java)
        initViews(viewModel)
        initObservers(viewModel)

        if(savedInstanceState == null) {
            viewModel.requestArtistDetails()
        }
    }

    private fun initViews(viewModel: ArtistDetailsViewModel) {
        artistBio.movementMethod = LinkMovementMethod.getInstance()
        artistName.text = viewModel.getName()
        Picasso.with(this).load(viewModel.getImageUrl()).into(backgroundImage)
    }

    private fun initObservers(viewModel: ArtistDetailsViewModel) {
        viewModel.getArtistDetails().observe(this, Observer<Artist>{
            artistBio.text = Html.fromHtml(it?.bio)
        })

        viewModel.getErrorState().observe(this, Observer<Throwable> {
            Toast.makeText(this, it?.message, Toast.LENGTH_SHORT).show() })

        viewModel.getLoadingState().observe(this, Observer<Boolean> {
            progressBar.visibility = if(it == true) View.VISIBLE else View.GONE
            artistBio.visibility = if(it == false) View.VISIBLE else View.GONE
        })
    }
}