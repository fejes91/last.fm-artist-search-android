package com.adamfejes.ui.artistDetails

import android.app.Application
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import com.adamfejes.entity.Artist
import com.adamfejes.ui.main.artistList.ArtistViewModelImpl

/**
 * Viewmodel interface for [ArtistDetailsActivity]
 */
abstract class ArtistDetailsViewModel : ViewModel() {
    abstract fun getArtistDetails(): LiveData<Artist>
    abstract fun requestArtistDetails()
    abstract fun getErrorState(): LiveData<Throwable>
    abstract fun getLoadingState(): LiveData<Boolean>
    abstract fun getName(): String
    abstract fun getImageUrl(): String?
}