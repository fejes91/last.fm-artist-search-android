package com.adamfejes.ui.main.testUtils

import com.adamfejes.LastFMSearchApp
import com.adamfejes.di.DaggerTestAppComponent
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

class TestApplication : LastFMSearchApp() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        val appComponent = DaggerTestAppComponent.builder().application(this).build()
        appComponent.inject(this)
        return appComponent
    }
}