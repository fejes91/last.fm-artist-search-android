package com.adamfejes.ui.main

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.UiController
import android.support.test.espresso.ViewAction
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.SearchView
import android.view.View
import com.adamfejes.lastfmArtistSearch.R
import com.adamfejes.ui.main.testUtils.RecyclerViewMatcher
import org.hamcrest.Matcher
import org.hamcrest.Matchers.allOf
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class MainActivityTest {
    @get:Rule
    var activityRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun testSearch() {
        onView(withId(R.id.searchView)).perform(typeToSearchViewTextAndSubmit("Any search term"))
        Thread.sleep(3000)
        onView(withRecyclerView(R.id.artistList).atPosition(0))
                .check(matches(hasDescendant(
                        withText("Artist1"))))
    }

    @Test
    fun testDetails() {
        onView(withId(R.id.searchView)).perform(typeToSearchViewTextAndSubmit("Any search term"))
        Thread.sleep(3000)
        onView(withRecyclerView(R.id.artistList).atPosition(0)).perform(click())
        onView(withId(R.id.artistName)).check(matches(withText("Artist1")))
    }

    private fun withRecyclerView(recyclerViewId: Int): RecyclerViewMatcher {
        return RecyclerViewMatcher(recyclerViewId)
    }

    fun typeToSearchViewTextAndSubmit(text: String): ViewAction {
        return object : ViewAction {
            override fun getConstraints(): Matcher<View> {
                //Ensure that only apply if it is a SearchView and if it is visible.
                return allOf(isDisplayed(), isAssignableFrom(SearchView::class.java))
            }

            override fun getDescription(): String {
                return "Change view text"
            }

            override fun perform(uiController: UiController, view: View) {
                (view as SearchView).setQuery(text, true)
            }
        }
    }
}